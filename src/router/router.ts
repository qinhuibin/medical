import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
//import Home from '../components/Home.vue'

/** 单条路由记录 */
/*
const routeTemp: RouteRecordRaw = {
  path: '路由地址',
  name: '路由名',
  component: '路由对应的组件',
  alias: '可选,string|string[],路由别名',
  redirect: '可选,重定向地址',
  children: '可选,子路由数组',
  meta: '可选,元字段-参数',
  props: '可选,开启动态路由传值',
  beforeEnter: '可选,进入该路由前执行什么操作'
}
*/
const routes: Array<RouteRecordRaw> = [
  {
    path: '/login',
    name: 'Login',
    component: () => import('../components/Login.vue'),
  },
  {
    path: '',
    name: 'Home',
    component: () => import('../components/Home.vue'),
    redirect: '/home',
    children: [
      {
        path: '/home',
        name: 'Home',
        component: () => import('../components/Home.vue'),
      }
    ]
  },
  // {
  //   path: '/home',
  //   name: 'Home',
  //   component: Home,
  // },
]

const router = createRouter({
  history: createWebHistory(),
  routes: routes,
})

export default router